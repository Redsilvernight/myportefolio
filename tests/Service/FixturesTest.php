<?php

namespace App\Tests\Service;

use App\DataFixtures\AppFixtures;
use App\Repository\ExperienceRepository;
use App\Repository\ProjectRepository;
use App\Repository\SkillRepository;
use App\Repository\UserPortefolioRepository;
use App\Repository\UserRepository;
use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class FixturesTest extends KernelTestCase
{
    public function setUp(): void
    {
        $kernel = self::bootKernel();
    }

    public function testFixturesUserPortefolio(): void
    {
        $userPortefolioRepository = static::getContainer()->get(UserPortefolioRepository::class);

        $this->assertEquals('1', $userPortefolioRepository->count([]));
    }

    public function testFixturesExperience(): void
    {
        $userPortefolioRepository = static::getContainer()->get(ExperienceRepository::class);

        $this->assertEquals('10', $userPortefolioRepository->count([]));
        $this->assertEquals('4', count($userPortefolioRepository->findBy(["is_dev" => true])));
        $this->assertEquals('6', count($userPortefolioRepository->findBy(["is_dev" => false])));
    }

    public function testFixturesSkill(): void
    {
        $skillRepository = static::getContainer()->get(SkillRepository::class);

        $this->assertEquals('15', $skillRepository->count([]));
        $this->assertEquals('5', count($skillRepository->findBy(["isSoftSkill" => true])));
        $this->assertEquals('10', count($skillRepository->findBy(["isSoftSkill" => false])));
    }

    public function testFixturesProject(): void
    {
        $projectRepository = static::getContainer()->get(ProjectRepository::class);

        $this->assertEquals('20', $projectRepository->count([]));
    }

    public function testFixturesUser(): void
    {
        $userRepository = static::getContainer()->get(UserRepository::class);

        $this->assertEquals('1', $userRepository->count([]));
    }
}
