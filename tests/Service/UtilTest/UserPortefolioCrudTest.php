<?php

namespace App\Tests\Service\UtilTest;

use App\Controller\Admin\DashboardController;
use App\Controller\Admin\UserPortefolioCrudController;
use App\Repository\UserPortefolioRepository;
use App\Repository\UserRepository;
use EasyCorp\Bundle\EasyAdminBundle\Test\AbstractCrudTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserPortefolioCrudTest extends AbstractCrudTestCase
{
    protected function getControllerFqcn(): string
    {
        return UserPortefolioCrudController::class;
    }

    protected function getDashboardFqcn(): string
    {
        return DashboardController::class;
    }

    public function testUserPortefolioIndex(): void
    {
        $userRepository = static::getContainer()->get(UserRepository::class);

        $testUser = $userRepository->findOneByEmail('florent-1@orange.fr');
        $this->client->loginUser($testUser);

        $this->client->request("GET", $this->generateIndexUrl());
        static::assertResponseIsSuccessful();
    }

    // public function testUserPortefolioUpdate(): void
    // {
    //     $userPortefolioRepository = $this->getContainer()->get(UserPortefolioRepository::class);
    //     $userRepository = static::getContainer()->get(UserRepository::class);

        
    //     $cv = new UploadedFile('/path/to/photo.jpg', 'photo.jpg', 'image/jpeg', 123);
        
    //     $testUser = $userRepository->findOneByEmail('florent-1@orange.fr');
    //     $this->client->loginUser($testUser);
        
    //     $crawler = $this->client->request("GET", $this->generateEditFormUrl($userPortefolioRepository->findAll([])[0]->getId()));
    //     static::assertResponseIsSuccessful();

    //     $buttonCrawlerNode = $crawler->selectButton('ea[newForm][btn]');
    //     $form = $buttonCrawlerNode->form();

    //     $form = $crawler->selectButton("ea[newForm][btn]")->form([
    //         "UserPortefolio[name]" => "Updated name",
    //         "UserPortefolio[title]" => "Updated title",
    //         "UserPortefolio[subtitle]" => "Updated subtitle",
    //         "UserPortefolio[description]" => "Updated description",
    //     ]);
        
    //     $this->client->submit($form);
    //     $this->assertResponseStatusCodeSame(Response::HTTP_FOUND);
    //     $this->client->followRedirect();
    //     static::assertResponseIsSuccessful();
    //     $this->assertRouteSame("admin");
    // }
}
