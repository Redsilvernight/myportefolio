<?php

namespace App\Controller;

use App\Entity\Project;
use App\Repository\ProjectRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProjectController extends AbstractController
{
    #[Route('/getProjects/{type}', name: 'app_getAllProjects')]
    public function getProjects(string $type, ProjectRepository $projectRepository): Response
    {
        $projects = $projectRepository->findBy(['is_perso' => 'Perso' === $type ? true : false]);

        return $this->render('shared/_allCardProjects.html.twig', [
            'projects' => $projects,
        ]);
    }

    #[Route('/showProject/{id}', name: 'app_showProject')]
    public function showProject(Project $project): Response
    {
        return $this->render('shared/_showProject.html.twig', [
            'project' => $project,
        ]);
    }
}
