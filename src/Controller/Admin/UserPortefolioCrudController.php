<?php

namespace App\Controller\Admin;

use App\Entity\UserPortefolio;
use App\Form\LinkType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class UserPortefolioCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return UserPortefolio::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name'),
            TextField::new('title'),
            TextField::new('subtitle'),
            TextField::new('description'),
            ImageField::new('cvName')->setUploadDir('/public/uploadedMedia/cv')->hideOnIndex(),
            BooleanField::new('available'),
            ArrayField::new('city'),
            DateField::new('birthday'),
            CollectionField::new('link')->setEntryType(LinkType::class)->setEntryIsComplex()->allowAdd()->allowDelete(),
            ImageField::new('imageName')->setUploadDir('/public/uploadedMedia')->hideOnIndex(),
        ];
    }
}
