<?php

namespace App\Controller\Admin;

use App\Entity\Experience;
use App\Entity\Project;
use App\Entity\Skill;
use App\Entity\UserPortefolio;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);

        return $this->redirect($adminUrlGenerator->setController(UserPortefolioCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Portefolio');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToCrud('Moi', 'fas fa-user', UserPortefolio::class);
        yield MenuItem::linkToCrud('Mes Expériences', 'fas fa-briefcase', Experience::class);
        yield MenuItem::linkToCrud('Mes Skill', 'fas fa-list', Skill::class);
        yield MenuItem::linkToCrud('Mes Projets', 'fas fa-project-diagram', Project::class);
    }
}
