<?php

namespace App\Controller\Admin;

use App\Entity\Project;
use App\Form\LinkType;
use App\Form\MediaType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;

class ProjectCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Project::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name'),
            TextareaField::new('description'),
            DateField::new('create_at'),
            BooleanField::new('is_perso'),
            AssociationField::new('skill')->setCrudController(SkillCrudController::class)->hideOnIndex(),
            CollectionField::new('link')->setEntryType(LinkType::class)->setEntryIsComplex()->allowAdd()->allowDelete()->hideOnIndex(),
            CollectionField::new('media')->setEntryType(MediaType::class)->hideOnIndex(),
        ];
    }
}
