<?php

namespace App\Controller\Admin;

use App\Entity\Experience;
use App\Form\MediaType;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ExperienceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Experience::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name'),
            TextareaField::new('description')->hideOnIndex(),
            DateField::new('started_at'),
            DateField::new('ended_at'),
            BooleanField::new('is_dev'),
            AssociationField::new('skill')->setCrudController(SkillCrudController::class)->hideOnIndex(),
            CollectionField::new('media')->setEntryType(MediaType::class)->hideOnIndex(),
        ];
    }
}
