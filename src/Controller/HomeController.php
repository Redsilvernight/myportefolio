<?php

namespace App\Controller;

use App\Entity\EmailContact;
use App\Form\EmailContactType;
use App\Repository\ExperienceRepository;
use App\Repository\ProjectRepository;
use App\Repository\SkillRepository;
use App\Repository\UserPortefolioRepository;
use App\Service\MailerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function about(UserPortefolioRepository $userPortefolioRepository, ProjectRepository $projectRepository, SkillRepository $skillRepository, ExperienceRepository $experienceRepository, Request $request, MailerService $mailerService): Response
    {
        $emailContact = new EmailContact();
        $emailForm = $this->createForm(EmailContactType::class, $emailContact);

        $emailForm->handleRequest($request);
        if ($emailForm->isSubmitted()) {
            $mailerService->sendMail($emailForm->getData());
            $this->addFlash('success', "Merci de m'avoir contacté ! Votre email à bien été envoyé.");
        }

        $userPortefolio = $userPortefolioRepository->findAll()[0];
        $projects = $projectRepository->findBy(['is_perso' => true]);
        $experiences = $experienceRepository->findBy(['is_dev' => true]);
        $skills = $skillRepository->findAll();

        return $this->render('home/about.html.twig', [
            'portefolio' => $userPortefolio,
            'now' => new \DateTime('now'),
            'projects' => $projects,
            'skills' => $skills,
            'experiences' => $experiences,
            'formContact' => $emailForm->createView(),
        ]);
    }
}
