<?php

namespace App\Controller;

use App\Repository\ExperienceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ExperienceController extends AbstractController
{
    #[Route('/getExperience/{type}', name: 'app_getAllExperiences')]
    public function getExperiences(string $type, ExperienceRepository $experienceRepository): Response
    {
        $experiences = $experienceRepository->findBy(['is_dev' => 'Dev' === $type ? true : false], ['started_at' => 'ASC']);

        return $this->render('shared/_experienceTimeline.html.twig', [
            'experiences' => $experiences,
        ]);
    }
}
