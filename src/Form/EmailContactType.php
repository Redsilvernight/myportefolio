<?php

namespace App\Form;

use App\Entity\EmailContact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmailContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('emailFrom', EmailType::class, [
                'label' => 'Votre adresse mail',
            ])
            ->add('emailSubject', TextType::class, [
                'label' => 'Sujet de votre message',
            ])
            ->add('emailContent', TextareaType::class, [
                'label' => 'Contenu de votre message',
                'attr' => [
                    'rows' => '10',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EmailContact::class,
        ]);
    }
}
