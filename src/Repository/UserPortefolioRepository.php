<?php

namespace App\Repository;

use App\Entity\UserPortefolio;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<UserPortefolio>
 *
 * @method UserPortefolio|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserPortefolio|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserPortefolio[]    findAll()
 * @method UserPortefolio[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserPortefolioRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserPortefolio::class);
    }

    //    /**
    //     * @return UserPortefolio[] Returns an array of UserPortefolio objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('u')
    //            ->andWhere('u.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('u.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?UserPortefolio
    //    {
    //        return $this->createQueryBuilder('u')
    //            ->andWhere('u.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}
