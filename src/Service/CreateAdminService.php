<?php

namespace App\Service;

use App\Entity\User;
use App\Entity\UserPortefolio;
use App\Repository\UserPortefolioRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class CreateAdminService
{
    public function __construct(private readonly UserRepository $userRepository, private readonly UserPasswordHasherInterface $userPasswordHasher, private readonly EntityManagerInterface $entityManager, private readonly UserPortefolioRepository $userPortefolioRepository)
    {
    }

    public function createAdmin(string $email, string $password): void
    {
        $user = $this->userRepository->findOneBy(['email' => $email]);

        if (!$user) {
            $user = new User();
            $user->setEmail($email)
                ->setPassword($this->userPasswordHasher->hashPassword($user, $password));
        }

        $user->setRoles(['ROLE_ADMIN']);

        $this->entityManager->persist($user);

        $this->createUserPortefolio($user);

        $this->entityManager->flush();
    }

    private function createUserPortefolio(User $user): void
    {
        $userPortefolio = $this->userPortefolioRepository->findOneBy(['user' => $user]);

        if (!$userPortefolio) {
            $userPortefolio = new UserPortefolio();
            $userPortefolio->setUser($user);
        }

        $userPortefolio->setBirthday(new \DateTime('2001-01-15'))
                    ->setDescription('Lorem ipsum dolor sit amet consectetur adipisicing elit. Nulla est veniam ea? Dignissimos provident unde cum reprehenderit dicta consequatur labore accusantium sapiente placeat! Asperiores rem quis obcaecati beatae, esse mollitia eaque accusantium est dicta? Totam reiciendis nihil commodi fuga autem error earum consectetur perferendis! Officia, deleniti quisquam sapiente ex maiores incidunt! Doloremque inventore tempora perspiciatis nulla ab ducimus mollitia voluptatibus qui aperiam iusto quis quos quibusdam quam, amet voluptatum laboriosam labore impedit laudantium magni nobis odit ad autem dolor. Vero velit, consectetur, eos dolorum delectus aliquid eveniet sequi nulla corporis quasi, officiis consequuntur pariatur similique culpa beatae aspernatur sapiente voluptates reiciendis eum modi cum ea eius. Facere, ex laborum temporibus porro recusandae quaerat reprehenderit! Facere quisquam facilis vitae, provident, in sed dolores hic quia deleniti quam consectetur nulla officiis possimus eveniet ipsum commodi laborum doloremque blanditiis. Exercitationem vel animi illo officiis nemo cumque quaerat praesentium quisquam blanditiis magnam? Iste, omnis!')
                    ->setName('Florent Sabio')
                    ->setTitle('Ton job ici')
                    ->setSubtitle('Ta spécialité là')
                    ->setImageName('profilImage.jpg')
                    ->setAvailable(1)
                    ->setCity(['Toulon']);

        $this->entityManager->persist($userPortefolio);
    }
}
