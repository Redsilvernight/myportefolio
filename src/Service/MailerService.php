<?php

namespace App\Service;

use App\Entity\EmailContact;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class MailerService
{
    public function __construct(private MailerInterface $mailer)
    {
    }

    public function sendMail(EmailContact $emailContact): void
    {
        $email = (new Email())
            ->from('Portefolio@contact.fr')
            ->to('redsilvernight@gmail.com')
            ->subject($emailContact->getEmailSubject())
            ->text('test de text')
            ->html(
                '<p>'.$emailContact->getEmailContent().'</p></br>
                <p>Contact: '.$emailContact->getEmailFrom().'</p>'
            );
        $this->mailer->send($email);

        return;
    }
}
