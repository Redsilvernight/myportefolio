<?php

namespace App\Entity;

use App\Repository\LinkRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LinkRepository::class)]
class Link
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $linkSelf = null;

    #[ORM\Column(length: 255)]
    private ?string $linkLabel = null;

    #[ORM\ManyToOne(inversedBy: 'link')]
    private ?UserPortefolio $userPortefolio = null;

    #[ORM\ManyToOne(inversedBy: 'link')]
    private ?Project $project = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLinkSelf(): ?string
    {
        return $this->linkSelf;
    }

    public function setLinkSelf(string $linkSelf): static
    {
        $this->linkSelf = $linkSelf;

        return $this;
    }

    public function getLinkLabel(): ?string
    {
        return $this->linkLabel;
    }

    public function setLinkLabel(string $linkLabel): static
    {
        $this->linkLabel = $linkLabel;

        return $this;
    }

    public function getUserPortefolio(): ?UserPortefolio
    {
        return $this->userPortefolio;
    }

    public function setUserPortefolio(?UserPortefolio $userPortefolio): static
    {
        $this->userPortefolio = $userPortefolio;

        return $this;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(?Project $project): static
    {
        $this->project = $project;

        return $this;
    }

    public function __toString(): string
    {
        return $this->linkSelf;
    }
}
