<?php

namespace App\Entity;

use App\Repository\EmailContactRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EmailContactRepository::class)]
class EmailContact
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $emailFrom = null;

    #[ORM\Column(length: 255)]
    private ?string $emailTo = null;

    #[ORM\Column(length: 255)]
    private ?string $emailSubject = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $emailContent = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $emailSendingDate = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmailFrom(): ?string
    {
        return $this->emailFrom;
    }

    public function setEmailFrom(string $emailFrom): static
    {
        $this->emailFrom = $emailFrom;

        return $this;
    }

    public function getEmailTo(): ?string
    {
        return $this->emailTo;
    }

    public function setEmailTo(string $emailTo): static
    {
        $this->emailTo = $emailTo;

        return $this;
    }

    public function getEmailSubject(): ?string
    {
        return $this->emailSubject;
    }

    public function setEmailSubject(string $emailSubject): static
    {
        $this->emailSubject = $emailSubject;

        return $this;
    }

    public function getEmailContent(): ?string
    {
        return $this->emailContent;
    }

    public function setEmailContent(string $emailContent): static
    {
        $this->emailContent = $emailContent;

        return $this;
    }

    public function getEmailSendingDate(): ?\DateTimeInterface
    {
        return $this->emailSendingDate;
    }

    public function setEmailSendingDate(\DateTimeInterface $emailSendingDate): static
    {
        $this->emailSendingDate = $emailSendingDate;

        return $this;
    }
}
