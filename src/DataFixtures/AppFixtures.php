<?php

namespace App\DataFixtures;

use App\Entity\Experience;
use App\Entity\Link;
use App\Entity\Media;
use App\Entity\Project;
use App\Entity\Skill;
use App\Entity\User;
use App\Entity\UserPortefolio;
use App\Repository\SkillRepository;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    protected $manager;
    protected Generator $faker;
    protected $skillRepository;
    protected $passwordHasheur;
    protected $userRepository;

    public function __construct(EntityManagerInterface $manager, SkillRepository $skillRepository, UserPasswordHasherInterface $passwordHasher, UserRepository $userRepository)
    {
        $this->manager = $manager;
        $this->faker = Factory::create('fr_FR');
        $this->skillRepository = $skillRepository;
        $this->passwordHasheur = $passwordHasher;
        $this->userRepository = $userRepository;
    }

    public function load(ObjectManager $manager): void
    {
        $this->loadUser();
        $this->loadSkill();

        $this->manager->flush();

        $this->loadUserPortefolio();

        $this->loadExperience();
        $this->loadProject();

        $manager->flush();
    }

    private function loadUser()
    {
        $user = new User();
        $user->setEmail('florent-1@orange.fr')
            ->setRoles(['ROLE_USER', 'ROLE_ADMIN'])
            ->setPassword($this->passwordHasheur->hashPassword($user, 'lemotdepasseduportefolio'));

        $this->manager->persist($user);
    }

    private function loadUserPortefolio()
    {
        $file = $this->createFile();
        $userPortefolio = new UserPortefolio();
        $userPortefolio->setName($this->faker->name())
                    ->setDescription($this->faker->text(500))
                    ->setBirthday($this->faker->dateTime())
                    ->setTitle('Développeur Web')
                    ->setSubtitle('Php/Symfony et autres')
                    ->setUser($this->userRepository->findAll()[0])
                    ->setAvailable(true)
                    ->addLink($this->createLink())
                    ->setImageName($file->getFilename())
                    ->setCity(['Toulon 83000', 'Lyon 69000'])
                    ->setImageSize($file->getSize());

        $this->manager->persist($userPortefolio);
    }

    private function loadExperience()
    {
        for ($forNewExperiences = 0; $forNewExperiences < 10; ++$forNewExperiences) {
            $experience = new Experience();
            $experience->setName($this->faker->jobTitle())
                        ->setDescription($this->faker->text(500))
                        ->setStartedAt($this->faker->dateTime())
                        ->setEndedAt($this->faker->dateTime())
                        ->setIsDev($forNewExperiences < 4 ? true : false);

            for ($nbrSkill = 0; $nbrSkill < mt_rand(1, 10); ++$nbrSkill) {
                $experience->addSkill($this->skillRepository->findAll()[mt_rand(0, $this->skillRepository->count([]) - 1)]);
            }

            for ($nbrMedia = 0; $nbrMedia < 1; ++$nbrMedia) {
                $media = $this->createMedia();
                $experience->addMedium($media);
            }

            $this->manager->persist($experience);
        }
    }

    private function loadSkill()
    {
        $skillList = ['Symfony' => 'symfony', 'Php' => 'php', 'Docker' => 'docker', 'Gitlab' => 'gitlab', 'Html' => 'html5', 'Css' => 'css3-alt', 'Python' => 'python', 'Mysql' => 'fa-2x fa-solid fa-database', 'Javascript' => 'js', 'Figma' => 'figma'];
        for ($forNewSkill = 0; $forNewSkill < 15; ++$forNewSkill) {
            $selectedSkillKey = array_rand($skillList);
            $newSkill = new Skill();
            $newSkill->setName($selectedSkillKey)
                    ->setIsSoftSkill($forNewSkill < 10 ? false : true);

            if ('Mysql' === $selectedSkillKey) {
                $newSkill->setIconPath('<i class="'.$skillList[$selectedSkillKey].'"></i>');
            } else {
                $newSkill->setIconPath('<i class="fa-2x fa-brands fa-'.$skillList[$selectedSkillKey].'"></i>');
            }

            $this->manager->persist($newSkill);
        }
    }

    private function loadProject()
    {
        for ($forNewProject = 0; $forNewProject < 20; ++$forNewProject) {
            $newProject = new Project();
            $newProject->setName($this->faker->jobTitle())
            ->setDescription($this->faker->text(1500))
            ->setCreateAt($this->faker->dateTime())
            ->setIsPerso($this->faker->boolean());

            for ($nbrLink = 0; $nbrLink < mt_rand(1, 3); ++$nbrLink) {
                $newProject->addLink($this->createLink());
            }

            for ($nbrSkill = 0; $nbrSkill < mt_rand(10, 25); ++$nbrSkill) {
                $newProject->addSkill($this->skillRepository->findAll()[mt_rand(0, $this->skillRepository->count([]) - 1)]);
            }

            for ($nbrMedia = 0; $nbrMedia < mt_rand(1, 3); ++$nbrMedia) {
                $media = $this->createMedia();
                $newProject->addMedium($media);
            }
            $this->manager->persist($newProject);
        }
    }

    private function createLink()
    {
        $link = new Link();
        $link->setLinkLabel($this->faker->jobTitle())
            ->setLinkSelf($this->faker->url());

        $this->manager->persist($link);

        return $link;
    }

    private function createMedia(): Media
    {
        $newFile = $this->createFile();
        $media = new Media();
        $media->setFileName($newFile->getFilename())
            ->setFileSize($newFile->getSize())
            ->setImageFile($newFile);

        $this->manager->persist($media);

        return $media;
    }

    private function createFile(): File
    {
        $fileUrl = 'https://picsum.photos/1920/1080';
        if (!is_dir('public/uploadedMedia')) {
            shell_exec('mkdir public/uploadedMedia');
        }

        $file = file_get_contents($fileUrl);
        $fileName = 'uploadedMedia-'.uniqid().'.png';
        $filePath = 'public/uploadedMedia/';
        file_put_contents($filePath.$fileName, $file);

        $file = new File($filePath.$fileName, $fileName);

        return $file;
    }
}
