import { blurProject, removeBlurProject } from './blur.js';

let projectToggle = document.querySelector("#toggle-project");
let experienceToggle = document.querySelector("#toggle-experience");
var cardProjects = document.querySelectorAll(".card--js-item");
var closeCardProjects = document.getElementById("project--show:close");
var divProject = document.querySelector("#project--show");


experienceToggle.addEventListener("change", viewExperience);
projectToggle.addEventListener("change", viewProject);

const PERSO = "Perso";
const PRO = "Pro";
const DEV = "Dev";
const OTHER = "Autres";
const axios = require('axios').default;

function viewExperience() {
    var input = this.children[0]
    var typeNameTag = document.getElementById("experience--type");
    var typeName = ""

    if (input.value === OTHER) {
        toggle(OTHER, "timeline", '/getExperience/');
        typeName = OTHER;
        input.value = DEV;
    } else {
        toggle(DEV, "timeline", '/getExperience/');
        console.log("switch dev");
        typeName = DEV;
        input.value = OTHER;
    }
    typeNameTag.innerHTML = typeName.charAt(0).toUpperCase() + typeName.slice(1);
}

function viewProject() {
    var input = this.children[0]
    var typeNameTag = document.getElementById("project--type");
    var typeName = ""

    if (input.value === PERSO) {
        toggle(PERSO, "slider", '/getProjects/');
        typeName = PERSO;
        input.value = PRO;
    } else {
        toggle(PRO, "slider", '/getProjects/');
        typeName = PRO;
        input.value = PERSO;
    }
    typeNameTag.innerHTML = typeName.charAt(0).toUpperCase() + typeName.slice(1);
}

function toggle(toggle, targetToggle, url) {
    var axiosDiv = document.getElementById(targetToggle);
    axios.get(url + toggle).then(function (response) {
        axiosDiv.innerHTML = response.data;
    });
    if (targetToggle === "timeline") {
        viewTarget();
    } else if (targetToggle === "slider") {
        callbackListener();
    }
    return;
}

function viewTarget() {
    setTimeout(function(){
        document.querySelectorAll("#timeline > li").forEach(element => {
            element.classList.add("show");
        });
    },100);
}

function callbackListener() {
    setTimeout(function(){
        cardProjects = document.querySelectorAll(".card--js-item")
        cardProjects.forEach(card => {
            card.addEventListener("click",loadProject);
        });
    },1000);
}

cardProjects.forEach(card => {
    card.addEventListener("click",loadProject);
});
closeCardProjects.addEventListener("click",closeProject);

function closeProject(e) {
    divProject.parentNode.classList.add("hidden");
    removeBlurProject();
}

function loadProject(e) {
    var id = e.currentTarget.id.slice(13);
    var url = "/showProject/" + id;
    axios.get(url).then(function (response) {
        divProject.innerHTML = response.data;
        divProject.parentNode.classList.remove("hidden");
        blurProject();
    });
}