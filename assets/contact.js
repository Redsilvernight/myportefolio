import { blurBody, removeBlurBody } from './blur.js';

var allContactBtn = document.querySelectorAll(".contact--btn");

allContactBtn.forEach(contactBtn => {
    contactBtn.addEventListener("click",showContactModal);
});

function showContactModal() {
    var contactModal = document.getElementById("contact--div");
    contactModal.classList.remove("hidden");
    blurBody();
    listenClose();
}

function listenClose() {
    var btnClose = document.getElementById("contact--div:close");
    btnClose.addEventListener("click",closeContact);
}

function closeContact() {
    var contactModal = document.getElementById("contact--div");
    contactModal.classList.add("hidden");
    removeBlurBody();
}