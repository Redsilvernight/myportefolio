function blurBody() {
    var bodyElement = document.querySelector("body");
    var sectionElement = document.querySelector("#section--content");
    sectionElement.classList.add("modalOpen");
    bodyElement.classList.add("modalOpenBody");
}

function removeBlurBody() {
    var bodyElement = document.querySelector("body");
    var sectionElement = document.querySelector("#section--content");
    sectionElement.classList.remove("modalOpen");
    bodyElement.classList.remove("modalOpenBody");
}

function blurProject() {
    var bodyElement = document.querySelector("body");
    var sectionElement = document.querySelector("#projects");
    var sliderElement = document.querySelector("#slider");
    jumpToProject();
    sliderElement.classList.remove("cards");
    sectionElement.classList.add("modalOpen");
    bodyElement.classList.add("modalOpenBody");
}

function removeBlurProject() {
    var bodyElement = document.querySelector("body");
    var sectionElement = document.querySelector("#projects");
    var sliderElement = document.querySelector("#slider");
    sliderElement.classList.add("cards");
    sectionElement.classList.remove("modalOpen");
    bodyElement.classList.remove("modalOpenBody");
}

function jumpToProject() {
    location.href = "#"+"projects";
}

export {blurBody, removeBlurBody, blurProject, removeBlurProject};