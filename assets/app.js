import './styles/app.css';
import './styles/carousel.css';
import './styles/skill.css';
import './styles/experience.css';
import './styles/project.css';

import AOS from 'aos';
import 'aos/dist/aos.css';


import './contact.js';
import './timeline.js';
import './toggle.js';

AOS.init({
  delay: 100,
});